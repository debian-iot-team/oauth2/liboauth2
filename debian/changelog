liboauth2 (2.1.0-1) unstable; urgency=medium

  * New upstream release
  * d/symbols: Add new symbols

 -- Nicolas Mora <babelouest@debian.org>  Wed, 12 Feb 2025 19:05:18 -0500

liboauth2 (2.0.0-1) unstable; urgency=medium

  * New upstream release
  * d/copyright: Update upstream license to Apache 2.0
  * d/patches: remove obsolete patches
  * d/control: Update standards version to 4.7.0 (no change)

 -- Nicolas Mora <babelouest@debian.org>  Tue, 27 Aug 2024 07:06:28 -0400

liboauth2 (1.6.3-1) unstable; urgency=medium

  * New upstream release

 -- Nicolas Mora <babelouest@debian.org>  Tue, 25 Jun 2024 08:09:51 -0400

liboauth2 (1.6.2-1) unstable; urgency=medium

  * New upstream release

 -- Nicolas Mora <babelouest@debian.org>  Thu, 06 Jun 2024 07:50:06 -0400

liboauth2 (1.6.1-1) unstable; urgency=medium

  * New upstream release
  * d/control: Use builddep pkgconf instead of pkg-config
  * d/copyright: Update copyright year

 -- Nicolas Mora <babelouest@debian.org>  Tue, 02 Apr 2024 19:30:31 -0400

liboauth2 (1.6.0-1) unstable; urgency=medium

  * New upstream release
  * d/patches: remove curl patch, now in upstream source code
  * d/patches: refresh test patch
  * d/symbols: Update symbols

 -- Nicolas Mora <babelouest@debian.org>  Wed, 06 Dec 2023 19:40:06 -0500

liboauth2 (1.5.2-1) unstable; urgency=medium

  * New upstream release
  * d/patches: remove test_timing patch, now in upstream source code

 -- Nicolas Mora <babelouest@debian.org>  Sat, 11 Nov 2023 13:16:27 -0500

liboauth2 (1.5.1-3) unstable; urgency=medium

  * Upload to unstable

 -- Nicolas Mora <babelouest@debian.org>  Sat, 01 Jul 2023 15:44:41 -0400

liboauth2 (1.5.1-2) experimental; urgency=medium

  * d/patches: Fix test timing issue

 -- Nicolas Mora <babelouest@debian.org>  Mon, 26 Jun 2023 07:17:07 -0400

liboauth2 (1.5.1-1) unstable; urgency=medium

  * New upstream release
  * d/symbols: Update symbols

 -- Nicolas Mora <babelouest@debian.org>  Sun, 25 Jun 2023 13:16:38 -0400

liboauth2 (1.4.5.4-1) unstable; urgency=medium

  * New upstream release
  * d/control: Update standards version to 4.6.2 (no change)
  * d/copyright: Update copyright years to 2023
  * d/liboauth2-apache0.symbols: Update symbols

 -- Nicolas Mora <babelouest@debian.org>  Sat, 21 Jan 2023 19:17:07 -0500

liboauth2 (1.4.5.2-1) unstable; urgency=medium

  * New upstream release

 -- Nicolas Mora <babelouest@debian.org>  Wed, 07 Dec 2022 07:17:46 -0500

liboauth2 (1.4.5.1-1) unstable; urgency=medium

  * New upstream release

 -- Nicolas Mora <babelouest@debian.org>  Mon, 22 Aug 2022 17:08:53 -0400

liboauth2 (1.4.5-1) unstable; urgency=medium

  * New upstream release
  * d/control: Update standards version to 4.6.1 (no change)

 -- Nicolas Mora <babelouest@debian.org>  Wed, 03 Aug 2022 07:34:52 -0400

liboauth2 (1.4.4.2-2) unstable; urgency=medium

  * Upload to unstable
  * d/test: re-enable autopkgtest

 -- Nicolas Mora <babelouest@debian.org>  Tue, 19 Apr 2022 17:22:44 -0400

liboauth2 (1.4.4.2-1) experimental; urgency=medium

  * New upstream release

 -- Nicolas Mora <babelouest@debian.org>  Sat, 16 Apr 2022 11:38:31 -0400

liboauth2 (1.4.4.1-3) experimental; urgency=medium

  * d/patches: move http test server to port 8080

 -- Nicolas Mora <babelouest@debian.org>  Sat, 16 Apr 2022 08:36:27 -0400

liboauth2 (1.4.4.1-2) experimental; urgency=medium

  * d/rules: Enable tests

 -- Nicolas Mora <babelouest@debian.org>  Thu, 07 Apr 2022 19:19:54 -0400

liboauth2 (1.4.4.1-1) unstable; urgency=medium

  * New upstream release
  * d/copyright: update year to 2022, update maintainer e-mail address

 -- Nicolas Mora <babelouest@debian.org>  Sat, 12 Mar 2022 08:07:32 -0500

liboauth2 (1.4.4-1) unstable; urgency=medium

  * New upstream release
  * d/rules: Remove flag -Wno-error=deprecated-declarations,
    now included in autoconf

 -- Nicolas Mora <babelouest@debian.org>  Thu, 30 Dec 2021 10:17:06 -0500

liboauth2 (1.4.3.2-4) unstable; urgency=medium

  * Fix build for openssl 3.0 (Closes: #1001356)
  * d/copyright: update year to 2021

 -- Nicolas Mora <babelouest@debian.org>  Wed, 08 Dec 2021 19:53:52 -0500

liboauth2 (1.4.3.2-3) unstable; urgency=medium

  * d/rules: disable tests in build, as they fail too

 -- Nicolas Mora <babelouest@debian.org>  Fri, 22 Oct 2021 07:33:54 -0400

liboauth2 (1.4.3.2-2) unstable; urgency=medium

  * Disable tests

 -- Nicolas Mora <babelouest@debian.org>  Thu, 21 Oct 2021 08:46:30 -0400

liboauth2 (1.4.3.2-1) unstable; urgency=medium

  * New upstream release
  * Add salsa-ci.yml
  * Set upstream metadata fields
  * d/liboauth2-0.symbols: update symbols
  * d/control: Update Standards-Version to 4.6.0 (no change)

 -- Nicolas Mora <babelouest@debian.org>  Wed, 20 Oct 2021 08:43:05 -0400

liboauth2 (1.4.0.1-1) unstable; urgency=medium

  * New upstream release
  * d/patches: remove size_t.patch and strncpy.patch, available upstream
  * d/liboauth2-0.symbols: update symbols
  * d/liboauth2-dev.install: remove config.h from install

 -- Nicolas Mora <babelouest@debian.org>  Fri, 25 Dec 2020 09:30:59 -0500

liboauth2 (1.3.0-8) unstable; urgency=medium

  * Upload to unstable
  * d/control: add dependencies for dev packages
  * d/control: Update Standards-Version to 4.5.1
  * d/rules: disable dh_auto_test

 -- Nicolas Mora <babelouest@debian.org>  Mon, 14 Dec 2020 10:18:59 -0500

liboauth2 (1.3.0-7) experimental; urgency=medium

  * d/rules: manually build then run test suite

 -- Nicolas Mora <babelouest@debian.org>  Tue, 15 Sep 2020 20:11:41 -0400

liboauth2 (1.3.0-6) experimental; urgency=medium

  * d/rules: re-enable dh_auto_test with -j1 and CK_FORK=no

 -- Nicolas Mora <babelouest@debian.org>  Tue, 15 Sep 2020 07:28:44 -0400

liboauth2 (1.3.0-5) unstable; urgency=medium

  * Upload to unstable

 -- Nicolas Mora <babelouest@debian.org>  Mon, 14 Sep 2020 07:23:12 -0400

liboauth2 (1.3.0-4) experimental; urgency=medium

  * d/rules: disable dh_auto_test

 -- Nicolas Mora <babelouest@debian.org>  Sun, 13 Sep 2020 17:53:48 -0400

liboauth2 (1.3.0-3) experimental; urgency=medium

  * d/tests: disable autopkgtest

 -- Nicolas Mora <babelouest@debian.org>  Sun, 13 Sep 2020 16:05:54 -0400

liboauth2 (1.3.0-2) UNRELEASED; urgency=medium

  * d/patch/size_t.patch: Fix size_t format in printf call

 -- Nicolas Mora <babelouest@debian.org>  Sat, 12 Sep 2020 15:28:25 -0400

liboauth2 (1.3.0-1) unstable; urgency=medium

  * Initial release (Closes: #969607)

 -- Nicolas Mora <babelouest@debian.org>  Sat, 05 Sep 2020 11:04:35 -0400
